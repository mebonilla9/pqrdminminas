/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.delegados;

import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.dao.CategoriasPqrdDAO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.CategoriasPqrd;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class CategoriasPqrdDelegado extends GenericoDelegado<CategoriasPqrd> {

    private final CategoriasPqrdDAO categoriasPqrdDAO;

    public CategoriasPqrdDelegado() throws PqrdMinMinasException {
        categoriasPqrdDAO = new CategoriasPqrdDAO(cnn);
        genericoDAO = categoriasPqrdDAO;
    }

}
