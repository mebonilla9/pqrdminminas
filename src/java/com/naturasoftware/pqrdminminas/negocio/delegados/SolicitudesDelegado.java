/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.delegados;

import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.conexion.BDConexion;
import com.naturasoftware.pqrdminminas.persistencia.dao.SolicitudesDAO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.Solicitudes;
import java.sql.SQLException;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class SolicitudesDelegado extends GenericoDelegado<Solicitudes> {

    private final SolicitudesDAO solicitudesDAO;

    public SolicitudesDelegado() throws PqrdMinMinasException {
        solicitudesDAO = new SolicitudesDAO(cnn);
        genericoDAO = solicitudesDAO;
    }
    
    public Solicitudes consultarSolicitudesUsuario(Long idUsuario) throws PqrdMinMinasException{
        try {
            return new SolicitudesDAO(cnn).solicitudesPorUsuario(idUsuario);
        } catch (SQLException ex) {
            BDConexion.rollback(cnn);
            throw new PqrdMinMinasException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
