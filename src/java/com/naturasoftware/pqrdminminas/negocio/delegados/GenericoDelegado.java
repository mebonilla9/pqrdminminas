/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.delegados;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.conexion.BDConexion;
import com.naturasoftware.pqrdminminas.persistencia.dao.IGenericoDAO;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public abstract class GenericoDelegado<T> {

    protected Connection cnn;
    protected IGenericoDAO genericoDAO;
    private boolean confirmar = true;

    public GenericoDelegado() throws PqrdMinMinasException {
        cnn = BDConexion.conectar();
    }

    public GenericoDelegado(Connection cnn) throws PqrdMinMinasException {
        this.cnn = cnn;
        this.confirmar = false;
    }

    public void insertar(T entidad) throws PqrdMinMinasException {
        try {
            genericoDAO.insertar(entidad);
            confirmar();
        } catch (SQLException ex) {
            BDConexion.rollback(cnn);
            throw new PqrdMinMinasException(EMensajes.ERROR_INSERTAR);
        } finally {
            desconectar();
        }
    }

    public void editar(T entidad) throws PqrdMinMinasException {
        try {
            genericoDAO.editar(entidad);
            confirmar();
        } catch (SQLException ex) {
            BDConexion.rollback(cnn);
            throw new PqrdMinMinasException(EMensajes.ERROR_MODIFICAR);
        } finally {
            desconectar();
        }
    }

    public List<T> consultar() throws PqrdMinMinasException {
        try {
            return genericoDAO.consultar();
        } catch (SQLException ex) {
            BDConexion.rollback(cnn);
            throw new PqrdMinMinasException(EMensajes.ERROR_CONSULTAR);
        } finally {
            desconectar();
        }
    }

    public T consultar(long id) throws PqrdMinMinasException {
        try {
            return (T) genericoDAO.consultar(id);
        } catch (SQLException ex) {
            BDConexion.rollback(cnn);
            throw new PqrdMinMinasException(EMensajes.ERROR_CONSULTAR);
        } finally {
            desconectar();
        }
    }

    private void confirmar() throws SQLException {
        if (confirmar) {
            cnn.commit();
        }
    }

    private void desconectar() {
        if (confirmar) {
            BDConexion.desconectar(cnn);
        }
    }

}
