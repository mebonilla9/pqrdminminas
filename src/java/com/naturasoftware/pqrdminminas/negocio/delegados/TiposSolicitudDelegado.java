/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.delegados;

import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.dao.TiposSolicitudDAO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.TiposSolicitud;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class TiposSolicitudDelegado extends GenericoDelegado<TiposSolicitud> {

    private final TiposSolicitudDAO tiposSolicitudDAO;

    public TiposSolicitudDelegado() throws PqrdMinMinasException {
        tiposSolicitudDAO = new TiposSolicitudDAO(cnn);
        genericoDAO = tiposSolicitudDAO;
    }

    public TiposSolicitud consultar(String idTsolicitud) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
