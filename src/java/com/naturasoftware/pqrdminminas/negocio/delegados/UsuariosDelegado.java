/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.delegados;

import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.conexion.BDConexion;
import com.naturasoftware.pqrdminminas.persistencia.dao.UsuariosDAO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.Usuarios;
import java.sql.SQLException;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class UsuariosDelegado extends GenericoDelegado<Usuarios> {

    private final UsuariosDAO usuariosDAO;

    public UsuariosDelegado() throws PqrdMinMinasException {
        usuariosDAO = new UsuariosDAO(cnn);
        genericoDAO = usuariosDAO;
    }

    public Usuarios usuarioDocumento(Long documento) throws PqrdMinMinasException {
        try {
            return new UsuariosDAO(cnn).consultarDocumento(documento);
        } catch (SQLException ex) {
            BDConexion.rollback(cnn);
            throw new PqrdMinMinasException(EMensajes.ERROR_CONSULTAR);
        }
    }
    
    public Usuarios usuarioLogin(Long documento, String correo) throws PqrdMinMinasException{
        try{
            return new UsuariosDAO(cnn).consultarUsuarioLogin(documento, correo);
        } catch(SQLException ex){
            BDConexion.rollback(cnn);
            throw new PqrdMinMinasException(EMensajes.ERROR_CONSULTAR);
        }
    }
}
