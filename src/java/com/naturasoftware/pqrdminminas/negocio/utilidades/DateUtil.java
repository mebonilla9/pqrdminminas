package com.naturasoftware.pqrdminminas.negocio.utilidades;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class DateUtil {

    public static java.sql.Date parseDate(java.util.Date fecha) {
        if (fecha == null) {
            return null;
        }
        return new java.sql.Date(fecha.getTime());
    }
}
