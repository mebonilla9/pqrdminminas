/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.servlets;

import com.naturasoftware.pqrdminminas.negocio.constantes.EAcciones;
import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.delegados.TiposSolicitudDelegado;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.dto.RespuestaDTO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.CategoriasPqrd;
import com.naturasoftware.pqrdminminas.persistencia.entidades.TiposSolicitud;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "TiposSolicitudServlet",
        urlPatterns = {"/tipossolicitud/insertar",
            "/tipossolicitud/modificar",
            "/tipossolicitud/consultar",
            "/tipossolicitud/listar"})
public class TiposSolicitudServlet extends GenericoServlet{

    @Override
    public RespuestaDTO procesar(HttpServletRequest request, EAcciones accion) throws PqrdMinMinasException {
        RespuestaDTO respuesta = null;
        System.out.println("Ingresando a: " + this.getServletName() + " con la accion: " + accion);
        switch (accion) {
            case INSERTAR:
                this.insertar(request);
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case MODIFICAR:
                this.modificar(request);
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case LISTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.listar());
                break;
            case CONSULTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.consultar(request));
        }
        return respuesta;
    }

    private void insertar(HttpServletRequest request) throws PqrdMinMinasException {
        String tsNombre = request.getParameter("tsNombre");
        String idCategoria = request.getParameter("idCategoria");
        TiposSolicitud tipoSolicitud = new TiposSolicitud();
        CategoriasPqrd categoria = new CategoriasPqrd();
        categoria.setIdCategoria(Long.parseLong(idCategoria));
        tipoSolicitud.setTsNombre(tsNombre);
        tipoSolicitud.setCategoria(categoria);
        new TiposSolicitudDelegado().insertar(tipoSolicitud);
    }

    private void modificar(HttpServletRequest request) throws PqrdMinMinasException {
        String idTsolicitud = request.getParameter("idTsolicitud");
        String tsNombre = request.getParameter("tsNombre");
        String idCategoria = request.getParameter("idCategoria");
        TiposSolicitud tipoSolicitud = new TiposSolicitud();
        CategoriasPqrd categoria = new CategoriasPqrd();
        categoria.setIdCategoria(Long.parseLong(idCategoria));
        tipoSolicitud.setIdTsolicitud(Long.parseLong(idTsolicitud));
        tipoSolicitud.setTsNombre(tsNombre);
        tipoSolicitud.setCategoria(categoria);
        new TiposSolicitudDelegado().editar(tipoSolicitud);
    }

    private List<TiposSolicitud> listar() throws PqrdMinMinasException {
        List<TiposSolicitud> listaTiposSolicitud = new TiposSolicitudDelegado().consultar();
        return listaTiposSolicitud;
    }

    private TiposSolicitud consultar(HttpServletRequest request) throws PqrdMinMinasException {
        String idTsolicitud = request.getParameter("idTsolicitud");
        TiposSolicitud tipoSolicitud = new TiposSolicitudDelegado().consultar(idTsolicitud);
        return tipoSolicitud;
    }
    
}
