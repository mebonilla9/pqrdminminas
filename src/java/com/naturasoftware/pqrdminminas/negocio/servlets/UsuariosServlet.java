/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.servlets;

import com.naturasoftware.pqrdminminas.negocio.constantes.EAcciones;
import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.delegados.UsuariosDelegado;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.dto.RespuestaDTO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.Usuarios;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "UsuariosServlet",
        urlPatterns = {"/usuarios/insertar",
            "/usuarios/modificar",
            "/usuarios/consultar",
            "/usuarios/documento",
            "/usuarios/listar",
            "/usuarios/login"})
public class UsuariosServlet extends GenericoServlet{

    @Override
    public RespuestaDTO procesar(HttpServletRequest request, EAcciones accion) throws PqrdMinMinasException {
        RespuestaDTO respuesta = null;
        System.out.println("Ingresando a: " + this.getServletName() + " con la accion: " + accion);
        switch (accion) {
            case INSERTAR:
                this.insertar(request);
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case MODIFICAR:
                this.modificar(request);
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case LISTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.listar());
                break;
            case CONSULTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.consultar(request));
                break;
            case CONSULTA_DOCUMENTO:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.consultarDoc(request));
                break;
            case LOGIN_USUARIO:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.iniciarSesion(request));
                break;
        }
        return respuesta;
    }

    private void insertar(HttpServletRequest request) throws PqrdMinMinasException {
        String usIdentificacion = request.getParameter("usIdentificacion");
        String usPrimerNombre = request.getParameter("usPrimerNombre");
        String usSegundoNombre = request.getParameter("usSegundoNombre").equals("")?null:request.getParameter("usSegundoNombre");
        String usPrimerApellido = request.getParameter("usPrimerApellido");
        String usSegundoApellido = request.getParameter("usSegundoApellido").equals("")?null:request.getParameter("usSegundoApellido");
        String usCorreo = request.getParameter("usCorreo");
        String usTelefono = request.getParameter("usTelefono").equals("")?null:request.getParameter("usTelefono");
        String usCelular = request.getParameter("usCelular");
        String usDireccion = request.getParameter("usDireccion").equals("")?null:request.getParameter("usDireccion");
        Usuarios usuario = new Usuarios();
        usuario.setUsIdentificacion(Long.parseLong(usIdentificacion));
        usuario.setUsPrimerNombre(usPrimerNombre);
        usuario.setUsSegundoNombre(usSegundoNombre!=null?usSegundoNombre:null);
        usuario.setUsPrimerApellido(usPrimerApellido);
        usuario.setUsSegundoApellido(usSegundoApellido!=null?usSegundoApellido:null);
        usuario.setUsCorreo(usCorreo);
        usuario.setUsTelefono(usTelefono!=null?Long.parseLong(usTelefono):null);
        usuario.setUsCelular(Long.parseLong(usCelular));
        usuario.setUsDireccion(usDireccion!=null?usDireccion:null);
        new UsuariosDelegado().insertar(usuario);
    }

    private void modificar(HttpServletRequest request) throws PqrdMinMinasException {
        String idUsuario = request.getParameter("idUsuario");
        String usIdentificacion = request.getParameter("usIdentificacion");
        String usPrimerNombre = request.getParameter("usPrimerNombre");
        String usSegundoNombre = request.getParameter("usSegundoNombre").equals("")?null:request.getParameter("usSegundoNombre");
        String usPrimerApellido = request.getParameter("usPrimerApellido");
        String usSegundoApellido = request.getParameter("usSegundoApellido").equals("")?null:request.getParameter("usSegundoApellido");
        String usCorreo = request.getParameter("usCorreo");
        String usTelefono = request.getParameter("usTelefono").equals("")?null:request.getParameter("usTelefono");
        String usCelular = request.getParameter("usCelular");
        String usDireccion = request.getParameter("usDireccion").equals("")?null:request.getParameter("usDireccion");
        Usuarios usuario = new Usuarios();
        usuario.setIdUsuario(Long.parseLong(idUsuario));
        usuario.setUsIdentificacion(Long.parseLong(usIdentificacion));
        usuario.setUsPrimerNombre(usPrimerNombre);
        usuario.setUsSegundoNombre(usSegundoNombre!=null?usSegundoNombre:null);
        usuario.setUsPrimerApellido(usPrimerApellido);
        usuario.setUsSegundoApellido(usSegundoApellido!=null?usSegundoApellido:null);
        usuario.setUsCorreo(usCorreo);
        usuario.setUsTelefono(usTelefono!=null?Long.parseLong(usTelefono):null);
        usuario.setUsCelular(Long.parseLong(usCelular));
        usuario.setUsDireccion(usDireccion!=null?usDireccion:null);
        new UsuariosDelegado().editar(usuario);
    }

    private List<Usuarios> listar() throws PqrdMinMinasException {
        List<Usuarios> listaUsuarios = new UsuariosDelegado().consultar();
        return listaUsuarios;
    }

    private Usuarios consultar(HttpServletRequest request) throws PqrdMinMinasException {
        String idUsuario = request.getParameter("idUsuario");
        Usuarios usuario = new UsuariosDelegado().consultar(Long.parseLong(idUsuario));
        return usuario;
    }

    private Usuarios consultarDoc(HttpServletRequest request) throws PqrdMinMinasException {
        String usIdentificacion = request.getParameter("us_identificacion");
        Usuarios usuario = new UsuariosDelegado().usuarioDocumento(Long.parseLong(usIdentificacion));
        return usuario;
    }
    
    private Usuarios iniciarSesion(HttpServletRequest request) throws PqrdMinMinasException{
        String usIdentificacion = request.getParameter("usIdentificacion");
        String usCorreo = request.getParameter("usCorreo");
        Usuarios usuario = new UsuariosDelegado().usuarioLogin(Long.parseLong(usIdentificacion), usCorreo);
        return usuario;
    }
    
}
