/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.servlets;

import com.naturasoftware.pqrdminminas.negocio.constantes.EAcciones;
import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.delegados.SolicitudesDelegado;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.persistencia.dto.RespuestaDTO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.Solicitudes;
import com.naturasoftware.pqrdminminas.persistencia.entidades.TiposSolicitud;
import com.naturasoftware.pqrdminminas.persistencia.entidades.Usuarios;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "SolicitudesServlet",
        urlPatterns = {"/solicitudes/insertar",
            "/solicitudes/modificar",
            "/solicitudes/consultar",
            "/solicitudes/listar",
            "/solicitudes/solusuario"})
public class SolicitudesServlet extends GenericoServlet {

    @Override
    public RespuestaDTO procesar(HttpServletRequest request, EAcciones accion) throws PqrdMinMinasException {
        RespuestaDTO respuesta = null;
        System.out.println("Ingresando a: " + this.getServletName() + " con la accion: " + accion);
        switch (accion) {
            case INSERTAR:
                this.insertar(request);
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case MODIFICAR:
                this.modificar(request);
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case LISTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.listar());
                break;
            case CONSULTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.consultar(request));
                break;
            case SOLICITUDES_USUARIO:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.solicitudesUsuario(request));
                break;
        }
        return respuesta;
    }

    private void insertar(HttpServletRequest request) throws PqrdMinMinasException {
        String soAsunto = request.getParameter("soAsunto").equals("") ? null : request.getParameter("soAsunto");
        String soTexto = request.getParameter("soTexto");
        String soNumeroRadicado = request.getParameter("soNumeroRadicado");
        String idTsolicitud = request.getParameter("idTsolicitud");
        String idUsuario = request.getParameter("idUsuario");
        Solicitudes solicitud = new Solicitudes();
        TiposSolicitud tipoSolicitud = new TiposSolicitud();
        Usuarios usuario = new Usuarios();
        solicitud.setSoAsunto(soAsunto != null ? soAsunto : null);
        solicitud.setSoTexto(soTexto);
        solicitud.setSoNumeroRadicado(!soNumeroRadicado.equals("") ? Long.parseLong(soNumeroRadicado) : null);
        tipoSolicitud.setIdTsolicitud(Long.parseLong(idTsolicitud));
        solicitud.setTsolicitud(tipoSolicitud);
        usuario.setIdUsuario(Long.parseLong(idUsuario));
        solicitud.setUsuario(usuario);
        Calendar calendar = Calendar.getInstance();
        solicitud.setSoFecha(new Date(calendar.getTimeInMillis()));
        new SolicitudesDelegado().insertar(solicitud);
    }

    private void modificar(HttpServletRequest request) throws PqrdMinMinasException {
        String idSolicitud = request.getParameter("idSolicitud");
        String soAsunto = request.getParameter("soAsunto").equals("") ? null : request.getParameter("soAsunto");
        String soTexto = request.getParameter("soTexto");
        String soNumeroRadicado = request.getParameter("soNumeroRadicado").equals("") ? null : request.getParameter("soNumeroRadicado");
        String idTsolicitud = request.getParameter("idTsolicitud");
        String idUsuario = request.getParameter("idUsuario");
        String soFecha = request.getParameter("soFecha");
        Solicitudes solicitud = new Solicitudes();
        TiposSolicitud tipoSolicitud = new TiposSolicitud();
        Usuarios usuario = new Usuarios();
        solicitud.setIdSolicitud(Long.parseLong(idSolicitud));
        solicitud.setSoAsunto(soAsunto != null ? soAsunto : null);
        solicitud.setSoTexto(soTexto);
        solicitud.setSoNumeroRadicado(soNumeroRadicado != null ? Long.parseLong(soNumeroRadicado) : null);
        tipoSolicitud.setIdTsolicitud(Long.parseLong(idTsolicitud));
        solicitud.setTsolicitud(tipoSolicitud);
        usuario.setIdUsuario(Long.parseLong(idUsuario));
        solicitud.setUsuario(usuario);
        solicitud.setSoFecha(Date.valueOf(soFecha));
        new SolicitudesDelegado().editar(solicitud);
    }

    private List<Solicitudes> listar() throws PqrdMinMinasException {
        List<Solicitudes> listaSolicitudes = new SolicitudesDelegado().consultar();
        return listaSolicitudes;
    }

    private Solicitudes consultar(HttpServletRequest request) throws PqrdMinMinasException {
        String idSolicitud = request.getParameter("idSolicitud");
        Solicitudes solucitud = new SolicitudesDelegado().consultar(Long.parseLong(idSolicitud));
        return solucitud;
    }

    private Object solicitudesUsuario(HttpServletRequest request) throws PqrdMinMinasException {
        String idUsuario = request.getParameter("idUsuario");
        Solicitudes solicitud = new SolicitudesDelegado().consultarSolicitudesUsuario(Long.parseLong(idUsuario));
        return solicitud;
    }

}
