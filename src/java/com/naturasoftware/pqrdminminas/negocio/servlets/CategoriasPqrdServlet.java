/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.negocio.servlets;

import com.naturasoftware.pqrdminminas.negocio.constantes.EAcciones;
import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;
import com.naturasoftware.pqrdminminas.negocio.delegados.CategoriasPqrdDelegado;
import com.naturasoftware.pqrdminminas.persistencia.dto.RespuestaDTO;
import com.naturasoftware.pqrdminminas.persistencia.entidades.CategoriasPqrd;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "CategoriasServlet",
        urlPatterns = {"/categoriaspqrd/insertar",
            "/categoriaspqrd/modificar",
            "/categoriaspqrd/consultar",
            "/categoriaspqrd/listar"})
public class CategoriasPqrdServlet extends GenericoServlet {

    @Override
    public RespuestaDTO procesar(HttpServletRequest request, EAcciones accion) throws PqrdMinMinasException {
        RespuestaDTO respuesta = null;
        System.out.println("Ingresando a: " + this.getServletName() + " con la accion: " + accion);
        switch (accion) {
            case INSERTAR:
                this.insertar(request);
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case MODIFICAR:
                this.modificar(request);
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case LISTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.listar());
                break;
            case CONSULTAR:
                respuesta = new RespuestaDTO(EMensajes.CONSULTO, this.consultar(request));
        }
        return respuesta;
    }

    private void insertar(HttpServletRequest request) throws PqrdMinMinasException {
        String catNombre = request.getParameter("catNombre");
        CategoriasPqrd categoria = new CategoriasPqrd();
        categoria.setCatNombre(catNombre);
        new CategoriasPqrdDelegado().insertar(categoria);

    }

    private void modificar(HttpServletRequest request) throws PqrdMinMinasException {
        String idCategoria = request.getParameter("idCategoria");
        String catNombre = request.getParameter("catNombre");
        CategoriasPqrd categoria = new CategoriasPqrd();
        categoria.setIdCategoria(Long.parseLong(idCategoria));
        categoria.setCatNombre(catNombre);
        new CategoriasPqrdDelegado().editar(categoria);
    }

    private List<CategoriasPqrd> listar() throws PqrdMinMinasException {
        List<CategoriasPqrd> listaCategorias = new CategoriasPqrdDelegado().consultar();
        return listaCategorias;
    }

    private CategoriasPqrd consultar(HttpServletRequest request) throws PqrdMinMinasException {
        String idCategoria = request.getParameter("idCategoria");
        CategoriasPqrd categoria = new CategoriasPqrdDelegado().consultar(Long.parseLong(idCategoria));
        return categoria;
    }

}
