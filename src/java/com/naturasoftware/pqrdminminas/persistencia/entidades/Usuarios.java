package com.naturasoftware.pqrdminminas.persistencia.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class Usuarios implements Serializable {

    private Long idUsuario;
    private Long usIdentificacion;
    private String usPrimerNombre;
    private String usSegundoNombre;
    private String usPrimerApellido;
    private String usSegundoApellido;
    private String usCorreo;
    private Long usTelefono;
    private Long usCelular;
    private String usDireccion;

    public Usuarios() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getUsIdentificacion() {
        return usIdentificacion;
    }

    public void setUsIdentificacion(Long usIdentificacion) {
        this.usIdentificacion = usIdentificacion;
    }

    public String getUsPrimerNombre() {
        return usPrimerNombre;
    }

    public void setUsPrimerNombre(String usPrimerNombre) {
        this.usPrimerNombre = usPrimerNombre;
    }

    public String getUsSegundoNombre() {
        return usSegundoNombre;
    }

    public void setUsSegundoNombre(String usSegundoNombre) {
        this.usSegundoNombre = usSegundoNombre;
    }

    public String getUsPrimerApellido() {
        return usPrimerApellido;
    }

    public void setUsPrimerApellido(String usPrimerApellido) {
        this.usPrimerApellido = usPrimerApellido;
    }

    public String getUsSegundoApellido() {
        return usSegundoApellido;
    }

    public void setUsSegundoApellido(String usSegundoApellido) {
        this.usSegundoApellido = usSegundoApellido;
    }

    public String getUsCorreo() {
        return usCorreo;
    }

    public void setUsCorreo(String usCorreo) {
        this.usCorreo = usCorreo;
    }

    public Long getUsTelefono() {
        return usTelefono;
    }

    public void setUsTelefono(Long usTelefono) {
        this.usTelefono = usTelefono;
    }

    public Long getUsCelular() {
        return usCelular;
    }

    public void setUsCelular(Long usCelular) {
        this.usCelular = usCelular;
    }

    public String getUsDireccion() {
        return usDireccion;
    }

    public void setUsDireccion(String usDireccion) {
        this.usDireccion = usDireccion;
    }

}
