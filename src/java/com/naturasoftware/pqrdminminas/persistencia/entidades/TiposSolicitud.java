package com.naturasoftware.pqrdminminas.persistencia.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class TiposSolicitud implements Serializable {

    private Long idTsolicitud;
    private String tsNombre;
    private CategoriasPqrd categoria;

    public TiposSolicitud() {
    }

    public Long getIdTsolicitud() {
        return idTsolicitud;
    }

    public void setIdTsolicitud(Long idTsolicitud) {
        this.idTsolicitud = idTsolicitud;
    }

    public String getTsNombre() {
        return tsNombre;
    }

    public void setTsNombre(String tsNombre) {
        this.tsNombre = tsNombre;
    }

    public CategoriasPqrd getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriasPqrd categoria) {
        this.categoria = categoria;
    }

}
