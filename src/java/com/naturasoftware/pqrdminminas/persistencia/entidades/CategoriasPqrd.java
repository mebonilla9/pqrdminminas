package com.naturasoftware.pqrdminminas.persistencia.entidades;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class CategoriasPqrd implements Serializable {

    private Long idCategoria;
    private String catNombre;

    public CategoriasPqrd() {
    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCatNombre() {
        return catNombre;
    }

    public void setCatNombre(String catNombre) {
        this.catNombre = catNombre;
    }

}
