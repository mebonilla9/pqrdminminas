package com.naturasoftware.pqrdminminas.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class Solicitudes implements Serializable {

    private Long idSolicitud;
    private String soAsunto;
    private String soTexto;
    private Long soNumeroRadicado;
    private TiposSolicitud tsolicitud;
    private Usuarios usuario;
    private Date soFecha;
    private String soResponse;
    private Date soTimeResponse;
    private Long usId;

    public Solicitudes() {
    }

    public Long getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Long idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getSoAsunto() {
        return soAsunto;
    }

    public void setSoAsunto(String soAsunto) {
        this.soAsunto = soAsunto;
    }

    public String getSoTexto() {
        return soTexto;
    }

    public void setSoTexto(String soTexto) {
        this.soTexto = soTexto;
    }

    public Long getSoNumeroRadicado() {
        return soNumeroRadicado;
    }

    public void setSoNumeroRadicado(Long soNumeroRadicado) {
        this.soNumeroRadicado = soNumeroRadicado;
    }

    public TiposSolicitud getTsolicitud() {
        return tsolicitud;
    }

    public void setTsolicitud(TiposSolicitud tsolicitud) {
        this.tsolicitud = tsolicitud;
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    public Date getSoFecha() {
        return soFecha;
    }

    public void setSoFecha(Date soFecha) {
        this.soFecha = soFecha;
    }

    public String getSoResponse() {
        return soResponse;
    }

    public void setSoResponse(String soResponse) {
        this.soResponse = soResponse;
    }

    public Date getSoTimeResponse() {
        return soTimeResponse;
    }

    public void setSoTimeResponse(Date soTimeResponse) {
        this.soTimeResponse = soTimeResponse;
    }

    public Long getUsId() {
        return usId;
    }

    public void setUsId(Long usId) {
        this.usId = usId;
    }

}
