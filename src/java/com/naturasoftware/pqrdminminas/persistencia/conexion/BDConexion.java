/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naturasoftware.pqrdminminas.persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.naturasoftware.pqrdminminas.negocio.constantes.EMensajes;
import com.naturasoftware.pqrdminminas.negocio.excepciones.PqrdMinMinasException;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class BDConexion {

    public static Connection conectar() throws PqrdMinMinasException {
        try {
            Class.forName("org.postgresql.Driver");
            Connection cnn = DriverManager.getConnection("jdbc:postgresql://190.146.253.75:5432/pqrd_minminas", "minminas", "minminas");
            cnn.setAutoCommit(false);
            return cnn;
        } catch (SQLException | ClassNotFoundException ex) {
            throw new PqrdMinMinasException(EMensajes.ERROR_CONEXION_BD);
        }
    }

    public static void desconectar(Connection cnn) {
        desconectar(cnn, null);
    }

    public static void desconectar(PreparedStatement ps) {
        desconectar(null, ps);

    }

    public static void desconectar(Connection cnn, PreparedStatement ps) {
        try {
            if (ps != null) {
                ps.close();
            }
            if (cnn != null) {
                cnn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(BDConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void rollback(Connection cnn) {
        try {
            cnn.rollback();
        } catch (SQLException ex) {
            Logger.getLogger(BDConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
