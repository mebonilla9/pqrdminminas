package com.naturasoftware.pqrdminminas.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import com.naturasoftware.pqrdminminas.persistencia.entidades.*;
import com.naturasoftware.pqrdminminas.persistencia.conexion.BDConexion;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class SolicitudesDAO implements IGenericoDAO<Solicitudes> {

    private final int ID = 1;
    private final Connection cnn;

    public SolicitudesDAO(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Solicitudes solicitudes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into solicitudes(so_asunto,so_texto,so_numero_radicado,id_tsolicitud,id_usuario,so_fecha,so_response,so_time_response,us_id) values (?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, solicitudes.getSoAsunto());
            sentencia.setObject(i++, solicitudes.getSoTexto());
            sentencia.setObject(i++, solicitudes.getSoNumeroRadicado());
            sentencia.setObject(i++, solicitudes.getTsolicitud().getIdTsolicitud());
            sentencia.setObject(i++, solicitudes.getUsuario().getIdUsuario());
            sentencia.setObject(i++, solicitudes.getSoFecha());
            sentencia.setObject(i++, solicitudes.getSoResponse());
            sentencia.setObject(i++, solicitudes.getSoTimeResponse());
            sentencia.setObject(i++, solicitudes.getUsId());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                solicitudes.setIdSolicitud(rs.getLong(ID));
            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Solicitudes solicitudes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update solicitudes set id_solicitud=?,so_asunto=?,so_texto=?,so_numero_radicado=?,id_tsolicitud=?,id_usuario=?,so_fecha=?,so_response=?,so_time_response=?,us_id=? where id_solicitud=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, solicitudes.getSoAsunto());
            sentencia.setObject(i++, solicitudes.getSoTexto());
            sentencia.setObject(i++, solicitudes.getSoNumeroRadicado());
            sentencia.setObject(i++, solicitudes.getTsolicitud().getIdTsolicitud());
            sentencia.setObject(i++, solicitudes.getUsuario().getIdUsuario());
            sentencia.setObject(i++, solicitudes.getSoFecha());
            sentencia.setObject(i++, solicitudes.getSoResponse());
            sentencia.setObject(i++, solicitudes.getSoTimeResponse());
            sentencia.setObject(i++, solicitudes.getUsId());
            sentencia.setObject(i++, solicitudes.getIdSolicitud());

            sentencia.executeUpdate();
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public List<Solicitudes> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Solicitudes> lista = new ArrayList<>();
        try {

            String sql = "select * from solicitudes";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                Solicitudes solicitudes = new Solicitudes();
                solicitudes.setIdSolicitud(rs.getLong("id_solicitud"));
                solicitudes.setSoAsunto(rs.getString("so_asunto"));
                solicitudes.setSoTexto(rs.getString("so_texto"));
                solicitudes.setSoNumeroRadicado(rs.getLong("so_numero_radicado"));
                TiposSolicitud tiposSolicitud = new TiposSolicitud();
                tiposSolicitud.setIdTsolicitud(rs.getLong("id_tsolicitud"));
                solicitudes.setTsolicitud(tiposSolicitud);
                Usuarios usuarios = new Usuarios();
                usuarios.setIdUsuario(rs.getLong("id_usuario"));
                solicitudes.setUsuario(usuarios);
                solicitudes.setSoFecha(rs.getDate("so_fecha"));
                solicitudes.setSoResponse(rs.getString("so_response"));
                solicitudes.setSoTimeResponse(rs.getDate("so_time_response"));
                solicitudes.setUsId(rs.getLong("us_id"));
                lista.add(solicitudes);

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Solicitudes consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Solicitudes obj = null;
        try {
            obj = new Solicitudes();
            String sql = "select * from solicitudes where id_solicitud=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj.setIdSolicitud(rs.getLong("id_solicitud"));
                obj.setSoAsunto(rs.getString("so_asunto"));
                obj.setSoTexto(rs.getString("so_texto"));
                obj.setSoNumeroRadicado(rs.getLong("so_numero_radicado"));
                TiposSolicitud tiposSolicitud = new TiposSolicitud();
                tiposSolicitud.setIdTsolicitud(rs.getLong("id_tsolicitud"));
                obj.setTsolicitud(tiposSolicitud);
                Usuarios usuarios = new Usuarios();
                usuarios.setIdUsuario(rs.getLong("id_usuario"));
                obj.setUsuario(usuarios);
                obj.setSoFecha(rs.getDate("so_fecha"));
                obj.setSoResponse(rs.getString("so_response"));
                obj.setSoTimeResponse(rs.getDate("so_time_response"));
                obj.setUsId(rs.getLong("us_id"));

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return obj;
    }
    
    public Solicitudes solicitudesPorUsuario(Long idUsuario) throws SQLException{
        PreparedStatement sentencia = null;
        Solicitudes obj = null;
        try {
            obj = new Solicitudes();
            String sql = "select * from solicitudes where id_usuario=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, idUsuario);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj.setIdSolicitud(rs.getLong("id_solicitud"));
                obj.setSoAsunto(rs.getString("so_asunto"));
                obj.setSoTexto(rs.getString("so_texto"));
                obj.setSoNumeroRadicado(rs.getLong("so_numero_radicado"));
                TiposSolicitud tiposSolicitud = new TiposSolicitud();
                tiposSolicitud.setIdTsolicitud(rs.getLong("id_tsolicitud"));
                obj.setTsolicitud(tiposSolicitud);
                Usuarios usuarios = new Usuarios();
                usuarios.setIdUsuario(rs.getLong("id_usuario"));
                obj.setUsuario(usuarios);
                obj.setSoFecha(rs.getDate("so_fecha"));
                obj.setSoResponse(rs.getString("so_response"));
                obj.setSoTimeResponse(rs.getDate("so_time_response"));
                obj.setUsId(rs.getLong("us_id"));

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return obj;
    }

}
