package com.naturasoftware.pqrdminminas.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import com.naturasoftware.pqrdminminas.persistencia.entidades.*;
import com.naturasoftware.pqrdminminas.persistencia.conexion.BDConexion;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class UsuariosDAO implements IGenericoDAO<Usuarios> {

    private final int ID = 1;
    private final Connection cnn;

    public UsuariosDAO(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Usuarios usuarios) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into usuarios(us_identificacion,us_primer_nombre,us_segundo_nombre,us_primer_apellido,us_segundo_apellido,us_correo,us_telefono,us_celular,us_direccion) values (?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, usuarios.getUsIdentificacion());
            sentencia.setObject(i++, usuarios.getUsPrimerNombre());
            sentencia.setObject(i++, usuarios.getUsSegundoNombre());
            sentencia.setObject(i++, usuarios.getUsPrimerApellido());
            sentencia.setObject(i++, usuarios.getUsSegundoApellido());
            sentencia.setObject(i++, usuarios.getUsCorreo());
            sentencia.setObject(i++, usuarios.getUsTelefono());
            sentencia.setObject(i++, usuarios.getUsCelular());
            sentencia.setObject(i++, usuarios.getUsDireccion());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                usuarios.setIdUsuario(rs.getLong(ID));
            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Usuarios usuarios) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update usuarios set id_usuario=?,us_identificacion=?,us_primer_nombre=?,us_segundo_nombre=?,us_primer_apellido=?,us_segundo_apellido=?,us_correo=?,us_telefono=?,us_celular=?,us_direccion=? where id_usuario=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, usuarios.getUsIdentificacion());
            sentencia.setObject(i++, usuarios.getUsPrimerNombre());
            sentencia.setObject(i++, usuarios.getUsSegundoNombre());
            sentencia.setObject(i++, usuarios.getUsPrimerApellido());
            sentencia.setObject(i++, usuarios.getUsSegundoApellido());
            sentencia.setObject(i++, usuarios.getUsCorreo());
            sentencia.setObject(i++, usuarios.getUsTelefono());
            sentencia.setObject(i++, usuarios.getUsCelular());
            sentencia.setObject(i++, usuarios.getUsDireccion());
            sentencia.setObject(i++, usuarios.getIdUsuario());

            sentencia.executeUpdate();
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public List<Usuarios> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Usuarios> lista = new ArrayList<>();
        try {

            String sql = "select * from usuarios";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                Usuarios usuarios = new Usuarios();
                usuarios.setIdUsuario(rs.getLong("id_usuario"));
                usuarios.setUsIdentificacion(rs.getLong("us_identificacion"));
                usuarios.setUsPrimerNombre(rs.getString("us_primer_nombre"));
                usuarios.setUsSegundoNombre(rs.getString("us_segundo_nombre"));
                usuarios.setUsPrimerApellido(rs.getString("us_primer_apellido"));
                usuarios.setUsSegundoApellido(rs.getString("us_segundo_apellido"));
                usuarios.setUsCorreo(rs.getString("us_correo"));
                usuarios.setUsTelefono(rs.getLong("us_telefono"));
                usuarios.setUsCelular(rs.getLong("us_celular"));
                usuarios.setUsDireccion(rs.getString("us_direccion"));
                lista.add(usuarios);

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Usuarios consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Usuarios obj = null;
        try {
            obj = new Usuarios();
            String sql = "select * from usuarios where id_usuario=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj.setIdUsuario(rs.getLong("id_usuario"));
                obj.setUsIdentificacion(rs.getLong("us_identificacion"));
                obj.setUsPrimerNombre(rs.getString("us_primer_nombre"));
                obj.setUsSegundoNombre(rs.getString("us_segundo_nombre"));
                obj.setUsPrimerApellido(rs.getString("us_primer_apellido"));
                obj.setUsSegundoApellido(rs.getString("us_segundo_apellido"));
                obj.setUsCorreo(rs.getString("us_correo"));
                obj.setUsTelefono(rs.getLong("us_telefono"));
                obj.setUsCelular(rs.getLong("us_celular"));
                obj.setUsDireccion(rs.getString("us_direccion"));

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return obj;
    }

    public Usuarios consultarDocumento(Long documento) throws SQLException {
        PreparedStatement sentencia = null;
        Usuarios obj = null;
        try {
            obj = new Usuarios();
            String sql = "select * from usuarios where us_identificacion=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, documento);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj.setIdUsuario(rs.getLong("id_usuario"));
                obj.setUsIdentificacion(rs.getLong("us_identificacion"));
                obj.setUsPrimerNombre(rs.getString("us_primer_nombre"));
                obj.setUsSegundoNombre(rs.getString("us_segundo_nombre"));
                obj.setUsPrimerApellido(rs.getString("us_primer_apellido"));
                obj.setUsSegundoApellido(rs.getString("us_segundo_apellido"));
                obj.setUsCorreo(rs.getString("us_correo"));
                obj.setUsTelefono(rs.getLong("us_telefono"));
                obj.setUsCelular(rs.getLong("us_celular"));
                obj.setUsDireccion(rs.getString("us_direccion"));

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return obj;
    }

    public Usuarios consultarUsuarioLogin(Long documento, String correo) throws SQLException {
        PreparedStatement sentencia = null;
        Usuarios obj = null;
        try {
            obj = new Usuarios();
            String sql = "select * from usuarios where us_identificacion=? and us_correo=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, documento);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj.setIdUsuario(rs.getLong("id_usuario"));
                obj.setUsIdentificacion(rs.getLong("us_identificacion"));
                obj.setUsPrimerNombre(rs.getString("us_primer_nombre"));
                obj.setUsSegundoNombre(rs.getString("us_segundo_nombre"));
                obj.setUsPrimerApellido(rs.getString("us_primer_apellido"));
                obj.setUsSegundoApellido(rs.getString("us_segundo_apellido"));
                obj.setUsCorreo(rs.getString("us_correo"));
                obj.setUsTelefono(rs.getLong("us_telefono"));
                obj.setUsCelular(rs.getLong("us_celular"));
                obj.setUsDireccion(rs.getString("us_direccion"));

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return obj;
    }

}
