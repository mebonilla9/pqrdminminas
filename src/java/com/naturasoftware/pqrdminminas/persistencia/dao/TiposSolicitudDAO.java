package com.naturasoftware.pqrdminminas.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import com.naturasoftware.pqrdminminas.persistencia.entidades.*;
import com.naturasoftware.pqrdminminas.persistencia.conexion.BDConexion;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class TiposSolicitudDAO implements IGenericoDAO<TiposSolicitud> {

    private final int ID = 1;
    private final Connection cnn;

    public TiposSolicitudDAO(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(TiposSolicitud tiposSolicitud) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into tipos_solicitud(ts_nombre,id_categoria) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, tiposSolicitud.getTsNombre());
            sentencia.setObject(i++, tiposSolicitud.getCategoria().getIdCategoria());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                tiposSolicitud.setIdTsolicitud(rs.getLong(ID));
            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(TiposSolicitud tiposSolicitud) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update tipos_solicitud set id_tsolicitud=?,ts_nombre=?,id_categoria=? where id_tsolicitud=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, tiposSolicitud.getTsNombre());
            sentencia.setObject(i++, tiposSolicitud.getCategoria().getIdCategoria());
            sentencia.setObject(i++, tiposSolicitud.getIdTsolicitud());

            sentencia.executeUpdate();
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public List<TiposSolicitud> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<TiposSolicitud> lista = new ArrayList<>();
        try {

            String sql = "select * from tipos_solicitud";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                TiposSolicitud tiposSolicitud = new TiposSolicitud();
                tiposSolicitud.setIdTsolicitud(rs.getLong("id_tsolicitud"));
                tiposSolicitud.setTsNombre(rs.getString("ts_nombre"));
                CategoriasPqrd categoriasPqrd = new CategoriasPqrd();
                categoriasPqrd.setIdCategoria(rs.getLong("id_categoria"));
                tiposSolicitud.setCategoria(categoriasPqrd);
                lista.add(tiposSolicitud);

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public TiposSolicitud consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        TiposSolicitud obj = null;
        try {
            obj = new TiposSolicitud();
            String sql = "select * from tipos_solicitud where id_tsolicitud=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj.setIdTsolicitud(rs.getLong("id_tsolicitud"));
                obj.setTsNombre(rs.getString("ts_nombre"));
                CategoriasPqrd categoriasPqrd = new CategoriasPqrd();
                categoriasPqrd.setIdCategoria(rs.getLong("id_categoria"));
                obj.setCategoria(categoriasPqrd);

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return obj;
    }

}
