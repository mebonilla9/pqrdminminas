package com.naturasoftware.pqrdminminas.persistencia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import com.naturasoftware.pqrdminminas.persistencia.entidades.*;
import com.naturasoftware.pqrdminminas.persistencia.conexion.BDConexion;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class CategoriasPqrdDAO implements IGenericoDAO<CategoriasPqrd> {

    private final int ID = 1;
    private final Connection cnn;

    public CategoriasPqrdDAO(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(CategoriasPqrd categoriasPqrd) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into categorias_pqrd(cat_nombre) values (?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, categoriasPqrd.getCatNombre());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                categoriasPqrd.setIdCategoria(rs.getLong(ID));
            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public void editar(CategoriasPqrd categoriasPqrd) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update categorias_pqrd set id_categoria=?,cat_nombre=? where id_categoria=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, categoriasPqrd.getCatNombre());
            sentencia.setObject(i++, categoriasPqrd.getIdCategoria());

            sentencia.executeUpdate();
        } finally {
            BDConexion.desconectar(sentencia);
        }
    }

    @Override
    public List<CategoriasPqrd> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<CategoriasPqrd> lista = new ArrayList<>();
        try {

            String sql = "select * from categorias_pqrd";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                CategoriasPqrd categoriasPqrd = new CategoriasPqrd();
                categoriasPqrd.setIdCategoria(rs.getLong("id_categoria"));
                categoriasPqrd.setCatNombre(rs.getString("cat_nombre"));
                lista.add(categoriasPqrd);

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public CategoriasPqrd consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        CategoriasPqrd obj = null;
        try {
            obj = new CategoriasPqrd();
            String sql = "select * from categorias_pqrd where id_categoria=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj.setIdCategoria(rs.getLong("id_categoria"));
                obj.setCatNombre(rs.getString("cat_nombre"));

            }
        } finally {
            BDConexion.desconectar(sentencia);
        }
        return obj;
    }

}
